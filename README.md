# FS1030_Individual_Project_Back End_2

# To Run Project

npm run start

# To Build and Deploy to GCP

($env:REACT_APP_BACKEND="https://gitlab-deploy-wbwty5b5hq-nn.a.run.app") -and (npm run build)

# Deployment

Dockerfile
Docker Compose
GCP Cloud Run

Enable Cloud Run API
Enable Google Container Registry API
Enable Compute Engine API

GItlab CI/CD Variables - BUCKET_NAME (name of GCP bucket) - REACT_APP_BACKEND - SERVICE_KEY_FILE

GCP mySQL

# Live Website

    http://rob.rob-wils.me/

# Example dotenv (including sample variables not used in the project)

PORT=1000
JWT_SECRET=example
saltRounds=example
privateKey=myexample
expirySeconds=example
DATABASE_ROOT_PASSWORD=example_password
DATABASE_HOST=localhost
DATABASE_USER=example_root
DATABASE_PASSWORD=example_password2
DATABASE_NAME=exampledb

# SQL Statements

Create Database new_db

Use new_db

CREATE TABLE adminUsers (
id INT UNSIGNED NOT NULL AUTO_INCREMENT,
email VARCHAR(255) NOT NULL DEFAULT '',
password VARCHAR(255) NOT NULL DEFAULT '',
PRIMARY KEY (id)
);

CREATE TABLE portfolio (
projectId INT UNSIGNED NOT NULL AUTO_INCREMENT,
projectName VARCHAR(255) NOT NULL DEFAULT '',
projectDesc VARCHAR(255) NOT NULL DEFAULT '',
year YEAR NOT NULL DEFAULT '1999',
PRIMARY KEY (projectId)
);

CREATE TABLE resume (
resumeId INT UNSIGNED NOT NULL AUTO_INCREMENT,
employerName VARCHAR(255) NOT NULL DEFAULT '',
jobTitle VARCHAR(255) NOT NULL DEFAULT '',
year YEAR NOT NULL DEFAULT '1999',
PRIMARY KEY (resumeId)
);

INSERT INTO resume (employerName, jobTitle, year) values ('fake employer','fake job title', '1999');

INSERT INTO portfolio (projectName, projectDesc, year) values ('fake project','the best project', '1999');
