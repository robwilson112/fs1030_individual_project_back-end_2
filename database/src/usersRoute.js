import express from 'express';
import jwt from 'jsonwebtoken'
import { dbQuery, existsUser, getLoginByEmail } from './util/dbhandler'
import { createHash, verify } from './util/bcrypt.js';

const router = express.Router();


/* Route authenticates users and creates a token for valid users 
Returns an error for invalid users or incomplete data */

router.post("/auth", (req, res, next) => {

    res.set('Access-Control-Allow-Origin', '*');
    
    let loginUser = { ...req.body };

    if (!loginUser.email || !loginUser.password) {
        // return 401 error is username or password doesn't exist
        return res.status(401).send({ "message": "incorrect credentials provided" });
    }

    try {
        let sql = `SELECT * FROM LOGIN WHERE EMAIL=?`;
        let params = loginUser.email;

        let users = getLoginByEmail(params).then((user) => {

            if (user == undefined || user.length == 0) {
                return res.status(401).send({ "message": "incorrect credentials provided" });
            }

            console.log(`user pass: ${loginUser.password} hash: ${user[0].password}`);

            verify(loginUser.password, user[0].password).then((match) => {

                if (match) {
                    let email = loginUser.email;
                    let token = jwt.sign({ email }, process.env.privateKey, { expiresIn: process.env.expirySeconds });
                    let permission = user[0].permission;
                    return res.status(201).send({ token, permission });
                }
                else {
                    // password does not match the password in our records
                    return res.status(401).send({ "message": "incorrect credentials provided" });
                }

            }).catch((err) => { console.log(err); })

            return user;
        }).catch((err) => { console.log(err); })



    } catch (err) {
        console.error(err.message);
        next(err);
    }
});


/* Route registers new users to the database 
Returns an error for incomplete data */
router.post("/register", (req, res, next) => {
    let newUser = { ...req.body };

    console.log(req.body);
    console.log(`Register : ${newUser.email}`);

    try {
        console.log(`Register : ${newUser.email} ${newUser.password}`);

        if (newUser.email == undefined || newUser.password == undefined) {
            // return 401 error is username or password doesn't exist
            return res.status(401).send({ "message": "incorrect credentials provided" });
        }

        let existUser = existsUser(newUser.email).then(function (results) {
            console.log(results);
            if (results) {
                return res.status(401).send({ "message": "User already exists for email" });
            }
            else {

                // Add the new user if it does not exist.
                createHash(newUser.password).then(hash => {
                    console.log(`pass: ${newUser.password} hash: ${hash}`);

                    let sql = "INSERT INTO adminUsers (email,password) VALUES (?,?)";
                    let params = [newUser.email, hash]
                    let emp = dbQuery(sql, params).then(function (results) { return results }).catch(function (err) { console.log(err); });

                    return res.status(200).send({ "message": "New user added" });

                }).catch((err) => { console.log(err); });

            }


            return results;
        }).catch(function (err) { console.log(err); });

        console.log(`exists : ${existUser}`);

    } catch (err) {
        console.error(err.message);
        next(err);
    }

});

export default router;