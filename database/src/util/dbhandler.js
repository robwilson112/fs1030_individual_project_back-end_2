import dotenv from 'dotenv'
import { connection as db } from './connection.js';


dotenv.config();

function dbQuery(databaseQuery, params) {
    return new Promise(data => {
        db.query(databaseQuery, params, function (error, result) {
            // change db->connection for your code
            if (error) {
                console.log(error);
                throw error;
            }

            
            try {
                console.log("In db query", result);

                data(result);

            } catch (error) {
                data({});
                throw error;
            }

        });
    });

}

// function dbQry(databaseQuery) {
//     return new Promise(data => {
//         db.query(databaseQuery, function (error, result) {
//             // change db->connection for your code
//             if (error) {
//                 console.log(error);
//                 throw error;
//             }

//             //console.log(result);
//             try {
//                 console.log("In db query", result);

//                 data(result);

//             } catch (error) {
//                 data({});
//                 throw error;
//             }

//         });
//     });

// }




async function getLoginByEmail(email) {
    
    let sql = `SELECT * FROM adminUsers WHERE EMAIL=?`;
    let user = [];
    let params = email;
    console.log(`email :"${params}"`)
    user = await dbQuery(sql, params).then(function (results) {
        console.log(results);
        return results;
    }).catch(function (err) { throw err; });
    console.log(`users : ${user[0]}`)
    return user;
}




async function existsUser(email) {

    let sql = `SELECT * FROM adminUsers WHERE EMAIL=?`;
    let params = email;
    let users = [];
    let exist = false;

    users = await dbQuery(sql, params).then(function (results) { return results; }).catch(function (err) { throw err; });
    console.log(users);


    if (users == undefined) {
        exist = false;
       
    }
    if (users.length > 0) {
        exist = true;
    }
    console.log(exist);
    return exist;
}

export { dbQuery, /*dbQry,*/ getLoginByEmail, existsUser };