 const mysql = require('mysql');

const connection = mysql.createConnection({
  socketPath: process.env.DATABASE_SOCKET,
  host     : process.env.DATABASE_HOST || 'localhost' ,
  user     : process.env.DATABASE_USER || 'root',
  password : process.env.DATABASE_PASSWORD || 'password',
  database : process.env.DATABASE_NAME || 'portfolio_site'
});



// const connection = require('knex')({
//   client: 'mysql',
//   connection: {
//     socketPath: process.env.DATABASE_SOCKET,
//     host : process.env.DATABASE_HOST,
//     user : process.env.DATABASE_USER,
//     password : process.env.DATABASE_PASSWORD,
//     database : process.env.DATABASE_NAME 
//   }
// });



connection.connect(

    function (err){
        if (err) {
            console.log(err)
        } else{
            console.log("mySQL DB is live!")
        }
    }

);
 

export { connection };