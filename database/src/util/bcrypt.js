import bcrypt from 'bcrypt';

let createHash = async (password) => {
  try {
    // more rounds, means slower, but harder to figure out the hash
    const hash = await bcrypt.hash(password, 10);
    return hash;
  } catch (err) {
    console.error(err)
  }
}

let verify = async (password, hash) => {
  try {
    const match = await bcrypt.compare(password, hash);

    return match;
  } catch (err) {
    console.error(err)
  }
}

export { createHash, verify };