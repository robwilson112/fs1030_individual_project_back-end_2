import express from 'express';
import {connection as db} from '../src/util/connection.js';


const router = express.Router();

// All resume GET API
router.get("/portfolio", function (req, res) {
    db.query(
        "SELECT * FROM portfolio",
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


// Project Update
router.put("/portfolio/:id", (req, res) => {
    const { projectName, projectDesc, year } = req.body;
    db.query(
        `UPDATE portfolio SET projectName="${projectName}", projectDesc="${projectDesc}", year="${year}" WHERE projectId=${req.params.id};`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.delete("/portfolio/:id", (req, res) => {
    db.query(
        `DELETE FROM portfolio WHERE projectId=${req.params.id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

// Add project
router.post("/portfolio", function (req, res) {
    db.query(
        "INSERT INTO portfolio (projectName, projectDesc, year) VALUES(?, ?, ?)",
        [req.body.projectName, req.body.projectDesc, req.body.year],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

export default router;