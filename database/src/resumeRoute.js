import express from 'express';
import {connection as db} from '../src/util/connection.js';


const router = express.Router();



// All resume GET API
router.get("/resume", function (req, res) {
    db.query(
        "SELECT * FROM resume",
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});


// Resume Update
router.put("/resume/:id", (req, res) => {
    const { employerName, jobTitle, year } = req.body;
    db.query(
        `UPDATE resume SET employerName="${employerName}", jobTitle="${jobTitle}", year="${year}" WHERE resumeId=${req.params.id};`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.delete("/resume/:id", (req, res) => {
    db.query(
        `DELETE FROM resume WHERE resumeId=${req.params.id}`,
        function (error, results, fields) {
            if (error) throw error;
            return res.status(200).send(results);
        }
    );
});

router.post("/resume", function (req, res) {
    db.query(
        "INSERT INTO resume (employerName, jobTitle, year) VALUES(?, ?, ?)",
        [req.body.employerName, req.body.jobTitle, req.body.year],
        function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        }
    );
});

export default router;