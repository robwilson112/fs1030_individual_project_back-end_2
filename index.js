// import express from 'express';
import dotenv from 'dotenv';
import router from './database/src/resumeRoute.js';
import routerProjects from './database/src/portfolioRoute.js';
import routeUser from './database/src/usersRoute.js';

const express = require('express');
const cors = require('cors');
const app = express();

// const corsOptions = {
// origin: function (origin, callback) {
// callback(null, true)
// }
// }



dotenv.config();
const PORT = process.env.PORT || 8080

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});

app.use(cors({
  origin: ['http://rob.rob-wils.me/', 'http://rob.rob-wils.me/login']
}));
app.options('*', cors());

app.use(express.json());

app.use("/api", router);
app.use("/users", routeUser);
app.use("/project", routerProjects);


// app.use((err, req, res, next) => {
//     if (res.headersSent) {
//         return next(err)
//     }
//     console.log("local error");
//     res.status(404).send(`{"message": "not found"}`);
// });

// app.use('*', (req, res, next) => {
//     console.log("global error");
//     res.status(404).send(`{"message": "not found"}`);

// });



// localhost:400/api/resume    will test all resume





app.listen(PORT, () => console.log(`server started on ${PORT}`))

export default app;